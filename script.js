var myApp = angular.module("counter", [])
    .controller("counterController", function ($scope) {
        $scope.counter = 0;
        $scope.increment = () => {
            $scope.counter++;
        }
        $scope.decrement = () => {
            $scope.counter--;
        }
    });